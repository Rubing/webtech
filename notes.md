Exercise 1

1. install git

2. clone
//clone from the link
git clone https://github.com/mattpe/git-intro.git

//change directory
cd git-intro/

//show the files in the server, here shows the file origin (optional)
git remote

3. go to BitBucket to create repository

4. remove and add
//remove the existing origin
git remove origin

//add origin with the link of the repository(you can find from the bitbucket set up page)
git remote add origin https://blablalbla.git

//check status(good habit)
git status

5. push the file
//-u tells the parameters, later no need to use. - - means there are no other options. - - all means all the modified files. origin means remote name for the remote repository
git push -u origin — -all

6. create a file, ending with .md (md refers to markdown file)

6+7. create a file and add to local git repository(Here file is created inside of the repository)
//create a file. md refers to markdown file.
touch notes.md

//list all the files in the repository
ls

//nano is a text editor command
nano notes.md

//shows in red font
git status

//add files to the stage. .(dot) means all the files in the repository(here is the git-intro)
git add .

//show in green font
git status

8. commit the changes
//commit(save) the file to somewhere(stage, unclear, local or cloud?) -m “” for leaving message.
git commit -m “messages that user leaves”

